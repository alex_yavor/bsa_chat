FROM node:10

COPY public ./public
COPY src ./src
COPY package*.json ./
COPY README.md ./
RUN npm install

CMD ["npm", "start"]
EXPOSE $PORT