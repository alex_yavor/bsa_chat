import React from 'react';
import './App.css';
import Chat from './containers/chat';
import Spinner from './components/spinner';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }
  componentDidMount() {
    const loading = false;
    //for example
    setTimeout(() => this.setState({ loading }), 1000)

  }

  render() {
    const { loading } = this.state;
    if (loading) {
      return <Spinner />
    }
    return (
      <Chat></Chat>
    )
  }
}


export default App;
