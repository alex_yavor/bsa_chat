import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

class EditMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageText: ""
        };
    }

    saveMessage = () => {
        let editedMessage = Object.assign({}, this.props.editableMessage);
        const messageText = this.state.messageText.trim();
        if (messageText === "") {
            return;
        }
        editedMessage.message = messageText;
        this.props.saveMessage(editedMessage);
    }

    handleChange = (event) => {
        const messageText = event.target.value;
        this.setState({ messageText })
    }

    render() {
        if (!this.props.show) {
            return null;
        }
        const editableMessage = this.props.editableMessage;
        return (
            <div className="backdrop" >
                <div className="modal" >

                    <div className="footer">
                        <div>Edit message</div>
                        <input
                            type="text"
                            placeholder='Type something'
                            defaultValue={editableMessage.message}
                            onChange={this.handleChange} />
                        <button
                            onClick={this.props.onClose}>
                            Close
                        </button>
                        <button
                            onClick={this.saveMessage}>
                            Save
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

EditMessage.propTypes = {
    onClose: PropTypes.func.isRequired,
    show: PropTypes.bool,
    editableMessage: PropTypes.object,
    saveMessage: PropTypes.func.isRequired
};

export default EditMessage;