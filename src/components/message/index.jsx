import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            likeCount: 0,
            isLiked: false
        };
    }

    likeMessage = () => {
        let { likeCount, isLiked } = this.state;
        isLiked = !isLiked;
        isLiked ? likeCount++ : likeCount--;
        this.setState({ likeCount, isLiked })
    }

    render() {
        const { id, avatar, message, created_at, user } = this.props;
        let likeCount = this.state.likeCount;
        const likeMessage = this.likeMessage;
        return (
            <div
                className="message"

                key={id}>
                <img
                    className="avatar"
                    src={avatar}
                    alt={user}
                />
                <div className="message-text">{message}</div>
                <div>{created_at}</div>

                <img
                    onClick={likeMessage}
                    className="icon hover"
                    src='like.svg'
                    alt="Like"
                />

                <span
                    onClick={likeMessage}
                    className="hover">
                    {likeCount}
                </span>
            </div>
        )
    }
}

Message.propTypes = {
    id: PropTypes.number,
    avatar: PropTypes.string,
    message: PropTypes.string,
    created_at: PropTypes.string,
    user: PropTypes.string
}


export default Message;