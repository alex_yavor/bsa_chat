import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

class MyMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            likeCount: 0,
        };
    }
    //unncomment all to like your own message
    // likeMessage = () => {
    //     let { likeCount, isLiked } = this.state;
    //     isLiked = !isLiked;
    //     isLiked ? likeCount++ : likeCount--;
    //     this.setState({ likeCount, isLiked })
    // }

    render() {
        const {
            message,
            created_at,
            id,
            deleteMessage,
            editMessage } = this.props;
        let likeCount = this.state.likeCount;
        // const likeMessage = this.likeMessage;
        return (
            <div
                key={id}
                className="message right"
            >
                <div className="message-text">{message}</div>

                <span>{created_at}</span>

                <div>
                    <img
                        // onClick={likeMessage}
                        className="icon hover"
                        src='like.svg'
                        alt="Like"
                    />

                    <span
                        // onClick={likeMessage}
                        className="hover">
                        {likeCount}
                    </span>
                    <span
                        className="delete"
                        onClick={() => deleteMessage(id)}
                    >delete</span>
                    <span
                        className="edit"
                        onClick={() => editMessage(id)}
                    >edit</span>
                </div>
            </div>
        )
    }
}

MyMessage.propTypes = {
    message: PropTypes.string,
    created_at: PropTypes.string,
    id: PropTypes.number,
    deleteMessage: PropTypes.func.isRequired,
    editMessage: PropTypes.func.isRequired
}


export default MyMessage;