import React from 'react';

import Header from '../header/index';
import MessageList from '../message.list';
import MessageInput from '../message.input';
import { getMessages } from '../../services/messageService';
import EditMessage from '../../components/editMessage';
import getFromattedTime from '../../helpers/timeFormat';

import './style.css';

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            newMessage: '',
            messageCount: 0,
            lastMessageDate: null,
            userCount: 0,
            lastId: 1,
            canEdit: false,
            editableMessage: {}
        };
    }

    componentDidMount = async () => {
        const messages = await getMessages();
        this.setState({ messages })
    }

    deleteMessage = (id) => {
        const OldMessages = this.state.messages;
        console.log('delid', id)
        const messages = OldMessages.filter(message => {
            return message.id !== id;
        })
        this.setState({ messages })
    }

    editMessage = (id) => {
        const canEdit = !this.state.canEdit;
        if (canEdit) {
            const editableMessage = this.state.messages.filter((message) => message.id === id)[0];
            this.setState({ editableMessage });
        }
        this.setState({ canEdit });
        console.log('canEdit', id)
    }

    saveMessage = (editedMessage) => {
        const oldMessages = this.state.messages.slice();
        const messages = oldMessages.map((message) => {
            return message.id === editedMessage.id ? editedMessage : message;
        })
        this.setState({ messages, canEdit: !this.state.canEdit, });
    }

    sendMessage = (message) => {
        const nowDate = getFromattedTime(new Date());
        let messages = this.state.messages.slice();
        const lastId = this.state.lastId;
        messages.push({
            created_at: nowDate,
            marked_read: false,
            message,
            user: "Kate",
            isMyMessage: true,
            id: lastId
        })
        console.log(lastId)
        this.setState({ messages, lastId: lastId + 1 })
    }

    render() {
        let { messages,
            messageCount,
            lastMessageDate,
            userCount,
            canEdit,
            editableMessage } = this.state;

        if (messages.length > 0) {
            messageCount = messages.length;
            const lastMessage = messages[messageCount - 1];
            lastMessageDate = getFromattedTime(lastMessage.created_at);
            let users = new Set(messages.map((message) => message.user));
            userCount = users.size;

        }
        return (

            <div className="chat" >
                <EditMessage show={canEdit}
                    onClose={this.editMessage}
                    saveMessage={this.saveMessage}
                    editableMessage={editableMessage}>
                </EditMessage>
                <Header
                    messageCount={messageCount}
                    lastMessageDate={lastMessageDate}
                    userCount={userCount}
                />
                <MessageList
                    deleteMessage={this.deleteMessage}
                    editMessage={this.editMessage}
                    messages={messages}

                />
                <MessageInput sendMessage={this.sendMessage} />
            </div >);
    }
}



export default Chat;
