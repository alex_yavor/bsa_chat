import React from 'react';
import PropTypes from 'prop-types';

import './style.css'

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            chatName: "My Chat"
        };
    }

    render() {
        const { messageCount, lastMessageDate, userCount } = this.props;
        return (
            <header className='header'>
                <div>My chat</div>
                <div className="participants">{userCount} participants</div>
                <div className="message-count">{messageCount} messages</div>
                <div className='last-message'>last message at <div>{lastMessageDate}</div></div>
            </header>
        )
    }
}

Header.propTypes = {
    messageCount: PropTypes.number,
    lastMessageDate: PropTypes.string,
    userCount: PropTypes.number
}

export default Header;