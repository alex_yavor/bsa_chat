import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageText: ""
        };
    }


    handleChange = (event) => {
        const messageText = event.target.value;
        this.setState({ messageText })
    }
    sendMessage = () => {
        const messageText = this.state.messageText.trim();
        if (messageText === "") return;
        this.props.sendMessage(messageText);
        this.setState({ messageText: "" })
    }
    render() {
        let messageText = this.state.messageText;

        return (
            <div className='message-input'>

                <input
                    type="text"
                    placeholder='Message'
                    value={messageText}
                    onChange={this.handleChange} />
                <button
                    className='send-message-button'
                    onClick={() => this.sendMessage()}>
                    Send</button>
            </div>
        );
    }
}

MessageInput.propTypes = {
    sendMessage:PropTypes.func.isRequired
}

export default MessageInput;