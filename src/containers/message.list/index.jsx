import React from 'react';
import Message from '../../components/message';
import MyMessage from '../../components/myMessage';
import DateLine from '../../components/dateLine';
import getFormattedTime from '../../helpers/timeFormat';
import PropTypes from 'prop-types';

class MessageList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageList: null,
        }
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    scrollToBottom() {
        const scrollHeight = this.messageList.scrollHeight;
        const height = this.messageList.clientHeight;
        const maxScrollTop = scrollHeight - height;
        this.messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    }


    renderMessages = () => {
        const { messages, deleteMessage, editMessage } = this.props;
        const listMessages = [];
        if (messages.length)
            listMessages.push(<DateLine key={messages[0].created_at} time={new Date(messages[0].created_at)} ></DateLine>)
        messages.forEach((message, messageIndex) => {
            let messageDiv;

            if (message.isMyMessage) {
                messageDiv = <MyMessage
                    id={message.id}
                    key={message.id}
                    created_at={getFormattedTime(message.created_at)}
                    message={message.message}
                    deleteMessage={deleteMessage}
                    editMessage={editMessage}
                />
            }
            else {
                messageDiv = <Message
                    avatar={message.avatar}
                    created_at={getFormattedTime(message.created_at)}
                    message={message.message}
                    key={message.id}
                    user={message.user}
                />
            }
            listMessages.push(messageDiv);
            if (messageIndex !== 0 && messageIndex + 1 !== messages.length) {
                const startTime = new Date(message.created_at).getTime();
                const endTime = new Date(messages[messageIndex + 1].created_at).getTime();
                const compareTime = Math.round((new Date(endTime).getTime() - new Date(startTime).getTime()) / 1000 / 60 / 60 / 24)
                if (compareTime >= 1) {
                    listMessages.push(<DateLine key={endTime} time={new Date(endTime)} ></DateLine>)
                }
            }
        })
        return listMessages;
    }

    render() {

        return (
            <div
                className='message-list'
                ref={messageList => { this.messageList = messageList; }}>
                {this.renderMessages()}
            </div>
        );
    }
}

MessageList.propTypes = {
    messages: PropTypes.array,
    deleteMessage: PropTypes.func.isRequired,
    editMessage: PropTypes.func.isRequired
}

export default MessageList;